
This simple package offers tools to handle Zernike Polynoms and the different indexing systems (as the
Noll system for instance).

The package offers object and non-object oriented functions. 




Install
-------

From pip:
    
    > pip install zernpol 
    
From sources:
    
    > git clone https://gricad-gitlab.univ-grenoble-alpes.fr/guieus/zernpol.git
    > cd zernpol 
    > python setup.py install 


Documentation 
-------------

Please visits: https://zernpol.readthedocs.io/en/latest/#



