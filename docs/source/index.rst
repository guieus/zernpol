.. zernpol documentation master file, created by
   sphinx-quickstart on Tue Oct 19 09:43:11 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
  :maxdepth: 3
  :caption: User Manual

  zernpol
 
 
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
